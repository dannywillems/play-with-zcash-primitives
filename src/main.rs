use std::convert::TryInto;
use zcash_primitives::pedersen_hash::Personalization;
use zcash_proofs::circuit::pedersen_hash as gadget_pedersen_hash;

use bellman::{
    gadgets::boolean::{AllocatedBit, Boolean},
    groth16, Circuit, ConstraintSystem, SynthesisError,
};
use bls12_381;
use rand::rngs::OsRng;

pub struct TestVector<'a> {
    pub personalization: Personalization,
    pub input_bits: Vec<u8>,
    pub hash_x: &'a str,
    pub hash_y: &'a str,
}

pub struct MyCircuit {
    preimage: Option<Vec<bool>>,
}

impl Circuit<bls12_381::Scalar> for MyCircuit {
    fn synthesize<CS: ConstraintSystem<bls12_381::Scalar>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        // Witness the bits of the preimage.
        let preimage_bits: Vec<Option<bool>> = if let Some(preimage) = self.preimage.clone() {
            preimage.into_iter().map(|b| Some(b)).collect()
        } else {
            // 180 (186 - 6) = preimage length (without the 6 first bits for the test vectors)
            vec![None; 180]
        };
        // For each bit of the input, we allocate a private variable.
        let preimage_bits_private: Vec<Boolean> = preimage_bits
            .into_iter()
            .enumerate()
            // Allocate each bit.
            .map(|(i, b)| AllocatedBit::alloc(cs.namespace(|| format!("preimage bit {}", i)), b))
            // Convert the AllocatedBits into Booleans (required for the sha256 gadget).
            .map(|b| b.map(Boolean::from))
            .collect::<Result<Vec<_>, _>>()?;

        // Compute hash = pedersen(preimage). It adds the computation constraints.
        let hash = gadget_pedersen_hash::pedersen_hash(
            cs.namespace(|| "Pedersen hash"),
            Personalization::NoteCommitment,
            &preimage_bits_private,
        )?;

        // We add two constraints for the coordinates. One is enough as we are
        // in the subgroup.
        let u: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_u();
        let v: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_v();
        // if self.preimage.is_some() {
        //     println!("In circuit - u: {:?}", u.get_value().unwrap());
        //     println!("In circuit - v: {:?}", v.get_value().unwrap());
        // }
        // println!("u: {:?}", u.get_value().unwrap());
        u.inputize(cs.namespace(|| "Inputize u coordinate"))?;
        v.inputize(cs.namespace(|| "Inputize v coordinate"))?;

        Ok(())
    }
}

fn test_pedersen_hash() {
    let v = TestVector {
        personalization: Personalization::NoteCommitment,
        input_bits: vec![
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1,
            1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1,
            0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1,
            0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0,
            1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
        ],
        hash_x: "599ab788360ae8c6d5bb7618aec37056d6227408d857fdc394078a3d7afdfe0f",
        hash_y: "4320c373da670e28d168f4ffd72b43208e8c815f40841682c57a3ee1d005a527",
    };

    let groth16_params = {
        let circuit = MyCircuit { preimage: None };
        groth16::generate_random_parameters::<bls12_381::Bls12, _, _>(circuit, &mut OsRng).unwrap()
    };
    let pvk = groth16::prepare_verifying_key(&groth16_params.vk);

    let circuit = MyCircuit {
        preimage: Some(
            v.input_bits
                .clone()
                .into_iter()
                .map(|bit| bit & 1u8 == 1u8)
                // We do not care about the first 6 elements. Don't know why yet...
                .skip(6)
                .collect::<Vec<bool>>(),
        ),
    };

    // Create a Groth16 proof with our parameters.
    let proof = groth16::create_random_proof(circuit, &groth16_params, &mut OsRng).unwrap();
    // println! {"Proof: a = {:?}, b = {:?}, c = {:?}", proof.a, proof.b, proof.c};

    // We reverse because the result hash_x and hash_y are given in big endian,
    // and bls12_381::Scalar::from_bytes supposes little endian.
    let mut hash_x_bytes =
        hex::decode(v.hash_x).expect("Unable to decode hex value of x coordinate");
    hash_x_bytes.reverse();
    let hash_x_slice: &[u8] = hash_x_bytes.as_slice();
    let hash_x_bytes: [u8; 32] = hash_x_slice
        .try_into()
        .expect("Incorrect length for x encoding");
    let result_x = bls12_381::Scalar::from_bytes(&hash_x_bytes).unwrap();

    let mut hash_y_bytes =
        hex::decode(v.hash_y).expect("Unable to decode hex value of y coordinate");
    hash_y_bytes.reverse();
    let hash_y_slice = hash_y_bytes.as_slice();
    let hash_y_bytes: [u8; 32] = hash_y_slice
        .try_into()
        .expect("Incorrect length for y encoding");
    let result_y = bls12_381::Scalar::from_bytes(&hash_y_bytes).unwrap();

    // println!("Precomputed result. X coordinate = {:?}, Y coordinate = {:?}", result_x, result_y);
    // Check the proof!
    let inputs = [result_x, result_y];
    groth16::verify_proof(&pvk, &proof, &inputs).unwrap();
}

fn main() {
    test_pedersen_hash();
}
