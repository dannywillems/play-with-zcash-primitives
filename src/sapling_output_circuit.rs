use bellman_circuit_exporter;
use bls12_381::Scalar;
use zcash_proofs::circuit::sapling::Output;

pub fn main() {
    let circuit = Output {
        value_commitment: None,
        payment_address: None,
        commitment_randomness: None,
        esk: None,
    };

    bellman_circuit_exporter::export_to_json::<Output, bls12_381::Scalar>(
        circuit,
        String::from("sapling-output-circuit.r1cs"),
        false,
    )
    .unwrap();
}
