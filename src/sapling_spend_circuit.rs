use bellman_circuit_exporter;
use bls12_381::Scalar;
use zcash_proofs::circuit::sapling::Spend;

pub fn main() {
    let spend = Spend {
        value_commitment: None,
        proof_generation_key: None,
        payment_address: None,
        commitment_randomness: None,
        ar: None,
        auth_path: vec![None],
        anchor: None,
    };

    bellman_circuit_exporter::export_to_json::<Spend, bls12_381::Scalar>(
        spend,
        String::from("sapling-spend-circuit.r1cs"),
        false,
    )
    .unwrap();
}
