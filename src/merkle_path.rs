use bellman::{
    gadgets::{boolean, num, Assignment},
    Circuit, ConstraintSystem, SynthesisError,
};
use ff::{Field, PrimeField};
use group::Curve;
use zcash_proofs::circuit::pedersen_hash as gadget_pedersen_hash;

use bellman::gadgets::test::*;
use bellman::groth16;
use rand::rngs::OsRng;
use rand_core::{RngCore, SeedableRng};
use rand_xorshift::XorShiftRng;
use std::time::Instant;
use zcash_primitives::pedersen_hash;

#[derive(Clone)]
pub struct MerkleTreeMembership {
    /// id of the user
    pub id: Option<bls12_381::Scalar>,

    /// The authentication path of the commitment in the tree
    pub auth_path: Vec<Option<(bls12_381::Scalar, bool)>>,

    /// The anchor; the root of the tree.
    pub anchor: Option<bls12_381::Scalar>,
}

// this circuit seems to work for paths of any lenght, but the length is fixed
// by the size of the auth_path passed as argument.

impl Circuit<bls12_381::Scalar> for MerkleTreeMembership {
    fn synthesize<CS: ConstraintSystem<bls12_381::Scalar>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        let tmp = self.id;
        let mut cur = num::AllocatedNum::alloc(cs.namespace(|| "id"), || Ok(*tmp.get()?))?;

        // Ascend the merkle tree authentication path
        for (i, e) in self.auth_path.into_iter().enumerate() {
            let cs = &mut cs.namespace(|| format!("merkle tree hash {}", i));

            // Determines if the current subtree is the "right" leaf at this
            // depth of the tree.
            let cur_is_right = boolean::Boolean::from(boolean::AllocatedBit::alloc(
                cs.namespace(|| "position bit"),
                e.map(|e| e.1),
            )?);

            // Witness the authentication path element adjacent
            // at this depth.
            let path_element =
                num::AllocatedNum::alloc(cs.namespace(|| "path element"), || Ok(e.get()?.0))?;

            // Swap the two if the current subtree is on the right
            let (ul, ur) = num::AllocatedNum::conditionally_reverse(
                cs.namespace(|| "conditional reversal of preimage"),
                &cur,
                &path_element,
                &cur_is_right,
            )?;

            // We don't need to be strict, because the function is
            // collision-resistant. If the prover witnesses a congruency,
            // they will be unable to find an authentication path in the
            // tree with high probability.
            let mut preimage = vec![];
            preimage.extend(ul.to_bits_le(cs.namespace(|| "ul into bits"))?);
            preimage.extend(ur.to_bits_le(cs.namespace(|| "ur into bits"))?);

            // Compute the new subtree value
            cur = gadget_pedersen_hash::pedersen_hash(
                cs.namespace(|| "computation of pedersen hash"),
                gadget_pedersen_hash::Personalization::MerkleTree(i),
                &preimage,
            )?
            .get_u()
            .clone(); // Injective encoding
        }
        {
            let real_anchor_value = self.anchor;

            // Allocate the "real" anchor that will be exposed.
            let rt = num::AllocatedNum::alloc(cs.namespace(|| "conditional anchor"), || {
                Ok(*real_anchor_value.get()?)
            })?;

            // (cur - rt) * 1 = 0
            cs.enforce(
                || "conditionally enforce correct root",
                |lc| lc + cur.get_variable() - rt.get_variable(),
                |lc| lc + CS::one(),
                |lc| lc,
            );

            // Expose the anchor
            rt.inputize(cs.namespace(|| "anchor"))?;
        }
        Ok(())
    }
}

fn generate_groth16_proof() {
    let start = Instant::now();

    let mut rng = XorShiftRng::from_seed([
        0x58, 0x62, 0xbe, 0x3d, 0x76, 0x3d, 0x31, 0x8d, 0x17, 0xdb, 0x37, 0x32, 0x54, 0x06, 0xbc,
        0xe5,
    ]);

    // 2:40 in debug mode
    // ~10 seconds in release mode:
    // Setup: 10314 ms
    // Prove: 1092 ms
    // Verify: 2 ms
    const TREE_DEPTH: usize = 32;

    let id = bls12_381::Scalar::random(&mut rng);
    let auth_path =
        vec![Some((bls12_381::Scalar::random(&mut rng), rng.next_u32() % 2 != 0)); TREE_DEPTH];

    let mut cur = id.clone();

    for (i, val) in auth_path.clone().into_iter().enumerate() {
        let (uncle, b) = val.unwrap();

        let mut lhs = cur;
        let mut rhs = uncle;

        if b {
            ::std::mem::swap(&mut lhs, &mut rhs);
        }

        let lhs = lhs.to_le_bits();
        let rhs = rhs.to_le_bits();

        cur = jubjub::ExtendedPoint::from(pedersen_hash::pedersen_hash(
            pedersen_hash::Personalization::MerkleTree(i),
            lhs.into_iter()
                .take(bls12_381::Scalar::NUM_BITS as usize)
                .chain(rhs.into_iter().take(bls12_381::Scalar::NUM_BITS as usize))
                .cloned(),
        ))
        .to_affine()
        .get_u();
    }

    let root = cur;

    let groth16_params = {
        let circuit = MerkleTreeMembership {
            id: None,
            auth_path: vec![None; TREE_DEPTH],
            anchor: None,
        };
        groth16::generate_random_parameters::<bls12_381::Bls12, _, _>(circuit, &mut OsRng).unwrap()
    };
    let pvk = groth16::prepare_verifying_key(&groth16_params.vk);

    let circuit = MerkleTreeMembership {
        id: Some(id),
        auth_path: auth_path,
        anchor: Some(root),
    };

    let mut cs = TestConstraintSystem::new();
    circuit.clone().synthesize(&mut cs).unwrap();
    assert!(cs.is_satisfied());

    let elapsed = start.elapsed();
    println!("Setup: {} ms", elapsed.as_millis());
    let start = Instant::now();

    let proof = groth16::create_random_proof(circuit, &groth16_params, &mut OsRng).unwrap();

    let elapsed = start.elapsed();
    println!("Prove: {} ms", elapsed.as_millis());
    let start = Instant::now();

    let public_inputs = [root];
    groth16::verify_proof(&pvk, &proof, &public_inputs).unwrap();

    let elapsed = start.elapsed();
    println!("Verify: {} ms", elapsed.as_millis());
}

fn extract_circuit() {
    const TREE_DEPTH: usize = 32;

    let mut rng = XorShiftRng::from_seed([
        0x58, 0x62, 0xbe, 0x3d, 0x76, 0x3d, 0x31, 0x8d, 0x17, 0xdb, 0x37, 0x32, 0x54, 0x06, 0xbc,
        0xe5,
    ]);

    let id = bls12_381::Scalar::random(&mut rng);
    let auth_path =
        vec![Some((bls12_381::Scalar::random(&mut rng), rng.next_u32() % 2 != 0)); TREE_DEPTH];

    let mut cur = id.clone();

    for (i, val) in auth_path.clone().into_iter().enumerate() {
        let (uncle, b) = val.unwrap();

        let mut lhs = cur;
        let mut rhs = uncle;

        if b {
            ::std::mem::swap(&mut lhs, &mut rhs);
        }

        let lhs = lhs.to_le_bits();
        let rhs = rhs.to_le_bits();

        cur = jubjub::ExtendedPoint::from(pedersen_hash::pedersen_hash(
            pedersen_hash::Personalization::MerkleTree(i),
            lhs.into_iter()
                .take(bls12_381::Scalar::NUM_BITS as usize)
                .chain(rhs.into_iter().take(bls12_381::Scalar::NUM_BITS as usize))
                .cloned(),
        ))
        .to_affine()
        .get_u();
    }

    let root = cur;

    {
        let circuit = MerkleTreeMembership {
            id: None,
            auth_path: vec![None; TREE_DEPTH],
            anchor: None,
        };
        bellman_circuit_exporter::export_to_json::<MerkleTreeMembership, bls12_381::Scalar>(
            circuit,
            String::from("merkle-path.r1cs"),
            false,
        )
        .unwrap()
    };

    {
        let circuit = MerkleTreeMembership {
            id: Some(id),
            auth_path: auth_path,
            anchor: Some(root),
        };
        bellman_circuit_exporter::export_to_json::<MerkleTreeMembership, bls12_381::Scalar>(
            circuit,
            String::from("merkle-path-with-evaluation.r1cs"),
            true,
        )
        .unwrap()
    };
}

fn main() {
    extract_circuit()
}
