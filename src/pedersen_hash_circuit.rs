use zcash_primitives::pedersen_hash::Personalization;
use zcash_proofs::circuit::pedersen_hash as gadget_pedersen_hash;

use bellman::{
    gadgets::boolean::{AllocatedBit, Boolean},
    Circuit, ConstraintSystem, SynthesisError,
};

use bellman_circuit_exporter;
use bls12_381;

pub struct TestVector<'a> {
    pub personalization: Personalization,
    pub input_bits: Vec<u8>,
    pub hash_x: &'a str,
    pub hash_y: &'a str,
}

pub struct MyCircuit {
    preimage: Option<Vec<bool>>,
}

impl Circuit<bls12_381::Scalar> for MyCircuit {
    fn synthesize<CS: ConstraintSystem<bls12_381::Scalar>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        // Witness the bits of the preimage.
        let preimage_bits: Vec<Option<bool>> = if let Some(preimage) = self.preimage.clone() {
            preimage.into_iter().map(|b| Some(b)).collect()
        } else {
            // 180 (186 - 6) = preimage length (without the 6 first bits for the test vectors)
            vec![None; 180]
        };
        // For each bit of the input, we allocate a private variable.
        let preimage_bits_private: Vec<Boolean> = preimage_bits
            .into_iter()
            .enumerate()
            // Allocate each bit.
            .map(|(i, b)| AllocatedBit::alloc(cs.namespace(|| format!("preimage bit {}", i)), b))
            // Convert the AllocatedBits into Booleans (required for the sha256 gadget).
            .map(|b| b.map(Boolean::from))
            .collect::<Result<Vec<_>, _>>()?;

        // Compute hash = pedersen(preimage). It adds the computation constraints.
        let hash = gadget_pedersen_hash::pedersen_hash(
            cs.namespace(|| "Pedersen hash"),
            Personalization::NoteCommitment,
            &preimage_bits_private,
        )?;

        // We add two constraints for the coordinates. One is enough as we are
        // in the subgroup.
        let u: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_u();
        let v: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_v();
        // if self.preimage.is_some() {
        //     println!("In circuit - u: {:?}", u.get_value().unwrap());
        //     println!("In circuit - v: {:?}", v.get_value().unwrap());
        // }
        // println!("u: {:?}", u.get_value().unwrap());
        u.inputize(cs.namespace(|| "Inputize u coordinate"))?;
        v.inputize(cs.namespace(|| "Inputize v coordinate"))?;

        Ok(())
    }
}

pub fn main() {
    let v = TestVector {
        personalization: Personalization::NoteCommitment,
        input_bits: vec![
            1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1,
            1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1,
            0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1,
            0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0,
            1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
        ],
        hash_x: "599ab788360ae8c6d5bb7618aec37056d6227408d857fdc394078a3d7afdfe0f",
        hash_y: "4320c373da670e28d168f4ffd72b43208e8c815f40841682c57a3ee1d005a527",
    };

    let circuit = MyCircuit {
        preimage: Some(
            v.input_bits
                .clone()
                .into_iter()
                .map(|bit| bit & 1u8 == 1u8)
                // We do not care about the first 6 elements. Don't know why yet...
                .skip(6)
                .collect::<Vec<bool>>(),
        ),
    };

    bellman_circuit_exporter::export_to_json::<MyCircuit, bls12_381::Scalar>(
        circuit,
        String::from("pedersen-hash-with-evaluation.r1cs"),
        true,
    )
    .unwrap();

    let circuit = MyCircuit { preimage: None };
    bellman_circuit_exporter::export_to_json::<MyCircuit, bls12_381::Scalar>(
        circuit,
        String::from("pedersen-hash.r1cs"),
        false,
    )
    .unwrap();
}
